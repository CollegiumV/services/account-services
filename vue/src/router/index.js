import Vue from 'vue';
import Router from 'vue-router';
import Account from '@/components/Account';
import Create from '@/components/Create';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    name: 'Account',
    component: Account,
  },
  {
    path: '/create/:token',
    name: 'Create',
    component: Create,
  },
  ],
});
