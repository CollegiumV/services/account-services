use errors::*;
use kerberos::{AuthClient, AuthClientBox};
use super::rocket;
use rocket::local::Client;
use rocket::http::ContentType;
use rocket::http::Status;

#[derive(Clone, Debug)]
struct FakeKrbClient {}
impl AuthClient for FakeKrbClient {
    fn add_principal(self, uid: &str, passwd: &str) -> Result<bool> {
        Ok(uid == "dino" && passwd == "password")
    }

    fn change_password(self, _: &str, _: &str) -> Result<()> {
        Ok(())
    }
}

fn new_client() -> Client {
    let auth_client: AuthClientBox = Box::new(FakeKrbClient {});
    Client::new(rocket().manage(auth_client)).expect("valid rocket instance")
}

#[test]
fn test_api_ping() {
    let client = new_client();
    let mut response = client.get("/api/ping").dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body_string(), Some("pong".into()));
}

#[test]
fn test_api_auth_success() {
    let client = new_client();
    let response = client
        .post("/api/auth")
        .body("{\"uid\" : \"dino\", \"password\" : \"password\"}")
        .header(ContentType::JSON)
        .dispatch();
    assert_eq!(response.status(), Status::Ok);
}

#[test]
fn test_api_auth_failure() {
    let client = new_client();
    let response = client
        .post("/api/auth")
        .body("{\"uid\" : \"dino\", \"password\" : \"wrong\"}")
        .header(ContentType::JSON)
        .dispatch();
    assert_eq!(response.status(), Status::Forbidden);
}
