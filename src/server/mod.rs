mod api;
#[cfg(test)]
mod tests;

use kerberos::{AuthClientBox, KrbClient};
use rocket;
use rocket::response::NamedFile;
use settings::Settings;
use std::path::{Path, PathBuf};

use errors::*;

#[get("/")]
fn index() -> Option<NamedFile> {
    NamedFile::open("static/index.html").ok()
}

#[get("/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

fn rocket() -> rocket::Rocket {
    rocket::ignite().mount("/", routes![index, files, api::ping, api::auth])
}

pub fn launch(settings: Settings) -> Result<()> {
    let krb_client: KrbClient =
        KrbClient::new(settings.auth.krb5).chain_err(|| "Could not initialize Kerberos Client")?;
    let krb_box: AuthClientBox = Box::new(krb_client);
    rocket().manage(krb_box).launch();
    Ok(())
}
