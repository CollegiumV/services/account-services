use frank_jwt::{encode, Algorithm};
use kerberos::AuthClientBox;
use rocket_contrib::Json;
use rocket::http::Status;
use rocket::response::status;
use rocket::State;

#[derive(Deserialize)]
pub struct AuthRequest {
    uid: String,
    password: String,
}

#[get("/api/ping")]
pub fn ping() -> &'static str {
    "pong"
}

#[post("/api/auth", data = "<login>")]
pub fn auth(
    login: Json<AuthRequest>,
    state: State<AuthClientBox>,
) -> Result<String, status::Custom<String>> {
    println!("State: {:?}", state);
    if login.uid == "dino" && login.password == "password" {
        let payload = json!({
            "uid" : login.uid,
            "admin" : true
        });
        let header = json!({});
        Ok(encode(header, &"test 123".to_string(), &payload, Algorithm::HS256).unwrap())
    } else {
        Err(status::Custom(
            Status::Forbidden,
            "Incorrect Username/Password".to_string(),
        ))
    }
}
