// Rust does not have solid bindings for kerberos
// We will probably be switching to FreeIPA soon
// Rather than dive way too deep into ffi, let's
// use subprocess calls as a temporary solution.

use settings::KerberosConfig;
use std::ffi::OsStr;
use std::fmt::Debug;
use std::path::PathBuf;

use errors::*;
use subprocess::{Exec, ExitStatus};

/// Rocket codegen breaks traits in route handlers.
/// The suggestion from IRC is to "wrap the generic innards in non-generic... outards?"
pub type AuthClientBox = Box<AuthClient>;

/// Using this as a trait allows us to swap out for other authentication backends pretty easily.
/// It also allows us to mock `KrbClient`
pub trait AuthClient: Debug + Send + Sync {
    fn add_principal(self, &str, &str) -> Result<bool>;
    fn change_password(self, &str, &str) -> Result<()>;
}

/// Information about the connection to the kerberos server
#[derive(Clone, Debug)]
pub struct KrbClient {
    /// Principal to log in as (i.e. websvc/admin@EXAMPLE.COM)
    ///
    /// Because of how `kadmin` parses this, it may be of the form:
    ///
    /// * `None` (in which case it will be inferred from the username and krb5.conf)
    /// * `websvc` (in which case the realm will be pulled from krb5.conf)
    /// * `websvc@EXAMPLE.COM` (is the only case in which '/admin' is not applied)
    /// * `websvc/admin@EXAMPLE.COM`
    principal: Option<String>,
    /// Authentication token, either a password or keytab.
    auth: AuthMethod,
    /// Server to connect to.  This may be inferred from krb5.conf.
    server: Option<String>,
}

#[derive(Clone, Debug)]
enum AuthMethod {
    Keytab(PathBuf),
    Password(String),
}

impl AuthClient for KrbClient {
    /// Add a new principal
    fn add_principal(self, uid: &str, password: &str) -> Result<bool> {
        match Exec::cmd("kadmin")
            .args(&self.krb_args()
                .chain_err(|| "Error determining kadmin args")?)
            .arg("add_principal")
            .arg("-pw")
            .arg(password)
            .arg(uid)
            .join()
        {
            Ok(ExitStatus::Exited(0)) => Ok(true),
            Ok(_) => Ok(false),
            _ => Err("Unable to exec kadmin".into()),
        }
    }

    /// Change the principal's password
    fn change_password(self, uid: &str, password: &str) -> Result<()> {
        self.run_kadmin(&["change_password", "-pw", password, uid])
            .chain_err(|| "Error running kadmin")?;
        Ok(())
    }
}

impl KrbClient {
    /// Set up a new Kerberos configuration standard
    pub fn new(config: KerberosConfig) -> Result<KrbClient> {
        // Get provided auth method
        let auth;
        if let Some(keytab) = config.keytab {
            auth = AuthMethod::Keytab(PathBuf::from(keytab));
        } else if let Some(password) = config.password {
            auth = AuthMethod::Password(password);
        } else {
            bail!("No keytab or password provided in config")
        };
        let client = KrbClient {
            principal: config.principal,
            auth: auth,
            server: config.server,
        };
        // I think once non-lexical lifetimes stablize, we can remove the clone
        match Exec::cmd("kadmin")
            .args(&client
                .clone()
                .krb_args()
                .chain_err(|| "Could not generate arguments")?)
            .arg("quit")
            .join()
            .chain_err(|| "Unable to login with provided credentials")?
        {
            ExitStatus::Exited(0) => Ok(client),
            _ => Err("Unable to login with provided credentials".into()),
        }
    }

    /// Call the kadmin binary
    pub(self) fn run_kadmin<S: AsRef<OsStr>>(self, args: &[S]) -> Result<()> {
        let mut p = Exec::cmd("kadmin")
            .args(&self.krb_args()
                .chain_err(|| "Error determining kadmin args")?)
            .args(args)
            .popen()
            .chain_err(|| "Could not exec kadmin")?;
        let (_, std_err) = p.communicate_bytes(None)
            .chain_err(|| "Could not open communication with kadmin process")?;
        let err = std_err.unwrap_or_else(Vec::new);
        let status = p.wait().chain_err(|| "Could not wait() the child process")?;
        match status {
            ExitStatus::Exited(0) => Ok(()),
            _ => Err(format!("Unable to exec kadmin: {:?}", err).into()),
        }
    }

    /// Generate the correct `kadmin` arguments from the connection parameters.
    pub(self) fn krb_args(self) -> Result<Vec<String>> {
        // This only really needs to be run once, not on every command
        let mut args = Vec::with_capacity(8);

        // Authentication args
        match self.auth {
            AuthMethod::Keytab(path) => args.extend_from_slice(&[
                "-k".to_owned(),
                "-t".to_owned(),
                path.to_str()
                    .chain_err(|| "Could not parse path")?
                    .to_owned(),
            ]),
            AuthMethod::Password(pass) => args.extend_from_slice(&["-w".to_owned(), pass]),
        };

        // Principal args
        if let Some(princ) = self.principal {
            args.extend_from_slice(&["-p".to_owned(), princ]);
        }

        // Server args
        if let Some(server) = self.server {
            args.extend_from_slice(&["-s".to_owned(), server]);
        }

        args.push("-q".to_owned());
        Ok(args)
    }
}

#[cfg(test)]
mod tests {
    use kerberos::{AuthMethod, KrbClient};
    use std::path::PathBuf;

    #[test]
    fn krb_args_minimal_password() {
        let client = KrbClient {
            principal: None,
            auth: AuthMethod::Password("pass".to_owned()),
            server: None,
        };
        let args = vec!["-w", "pass", "-q"];
        assert_eq!(client.krb_args().unwrap(), args);
    }

    #[test]
    fn krb_args_maximal_keytab() {
        let client = KrbClient {
            principal: Some("username".to_owned()),
            auth: AuthMethod::Keytab(PathBuf::from("/tmp/keytab")),
            server: Some("kerberos.example.test".to_owned()),
        };

        let args = vec![
            "-k",
            "-t",
            "/tmp/keytab",
            "-p",
            "username",
            "-s",
            "kerberos.example.test",
            "-q",
        ];
        assert_eq!(client.krb_args().unwrap(), args);
    }
}
