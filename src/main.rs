#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate config;
#[macro_use]
extern crate error_chain;
extern crate frank_jwt;
extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
extern crate subprocess;

mod kerberos;
mod settings;
mod server;

mod errors {
    error_chain!{}
}

use settings::Settings;

fn main() {
    use std::io::Write;
    use error_chain::ChainedError; // trait which holds `display_chain`
    let stderr = &mut ::std::io::stderr();
    let errmsg = "Error writing to stderr";

    let settings = match Settings::new() {
        Ok(settings) => settings,
        Err(ref e) => {
            writeln!(stderr, "Config Error: {}", e).expect(errmsg);
            std::process::exit(1)
        }
    };
    if let Err(ref e) = server::launch(settings) {
        writeln!(stderr, "{}", e.display_chain()).expect(errmsg);
        std::process::exit(1);
    }
}
