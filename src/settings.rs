use std::collections::HashMap;
use config::{Config, ConfigError, Environment, File};

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub server: ServerConfig,
    pub auth: AuthConfig,
    pub user: UserConfig,
    pub admin: AdminConfig,
}

#[derive(Debug, Deserialize)]
pub struct ServerConfig {
    pub address: String,
    pub port: u16,
    pub secret: String,
}

#[derive(Debug, Deserialize)]
pub struct AuthConfig {
    pub provider: String,
    pub krb5: KerberosConfig,
    pub ldap: LdapConfig,
    pub freeipa: FreeIpaConfig,
}

#[derive(Debug, Deserialize)]
pub struct KerberosConfig {
    pub server: Option<String>,
    pub principal: Option<String>,
    pub password: Option<String>,
    pub keytab: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct LdapConfig {
    pub server: String,
    pub port: u16,
    pub starttls: bool,
    pub check_cert: bool,
    pub bind_dn: String,
    pub bind_pw: String,
    pub base_dn: String,
    pub username_field: String,
}

#[derive(Debug, Deserialize)]
pub struct FreeIpaConfig {
    pub server: String,
    pub port: u16,
    pub username: String,
    pub password: String,
    pub keytab: String,
}

#[derive(Debug, Deserialize)]
pub struct UserConfig {
    pub allowed_list: String,
    pub user_manageable_fields: Option<Vec<String>>,
    pub user_defaults: Option<HashMap<String, String>>,
}

#[derive(Debug, Deserialize)]
pub struct AdminConfig {
    pub admin_groups: Vec<String>,
    pub admin_manageable_fields: Vec<String>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut settings = Config::new();
        settings
            .set_default("server.address", "0.0.0.0")?
            .set_default("server.port", 8585)?
            .set_default("server.secret", "change_me")?
            .set_default("auth.provider", "krb5+ldap")?
            .set_default("auth.ldap.server", "ldap://localhost")?
            .set_default("auth.ldap.port", 389)?
            .set_default("auth.ldap.starttls", false)?
            .set_default("auth.ldap.check_cert", true)?
            .set_default("auth.ldap.bind_dn", "cn=account-service,dc=example,dc=com")?
            .set_default("auth.ldap.bind_pw", "password")?
            .set_default("auth.ldap.base_dn", "ou=user,dc=example,dc=com")?
            .set_default("auth.ldap.username_field", "uid")?
            .set_default("auth.freeipa.server", "localhost")?
            .set_default("auth.freeipa.port", 389)?
            .set_default("auth.freeipa.keytab", "/etc/krb5.keytab")?
            .set_default("auth.freeipa.username", "account-service")?
            .set_default("auth.freeipa.password", "password")?
            .set_default("user.allowed_list", "")?
            .set_default(
                "user.user_manageable_fields",
                vec![
                    "password",
                    "email",
                    "ssh_keys",
                    "desktop_environment",
                    "login_shell",
                ],
            )?
            .set_default(
                "admin.admin_groups",
                vec!["cn=account-admins,ou=Groups,dc=example,dc=com"],
            )?
            .set_default(
                "admin.admin_manageable_fields",
                vec![
                    "first_name",
                    "last_name",
                    "password",
                    "email",
                    "ssh_keys",
                    "desktop_environment",
                    "login_shell",
                ],
            )?
            .merge(File::with_name("configs/app.toml"))?
            .merge(Environment::with_prefix("APP"))?;

        settings.try_into()
    }
}
